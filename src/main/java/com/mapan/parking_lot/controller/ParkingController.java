/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mapan.parking_lot.controller;

import com.mapan.parking_lot.dao.ParkingDAO;
import com.mapan.parking_lot.model.Parking;
import com.mapan.parking_lot.model.Parkings;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author edyprayitno
 */
public class ParkingController {
    
    public ParkingController(){
    
    }
    
    int slotNumber = 0;
    
    private ParkingDAO parkingDao = new ParkingDAO();
    
    public StringBuilder parkingCommand(String input) throws IOException{
        String[] commandString;
        commandString = getCommandLine(input);
        String command = commandString[0];
        StringBuilder parkingReturn = new StringBuilder();
        if ("exit".equals(command)) {
            System.exit(0);
        }

        if("park".equals(command)){
            parkingReturn = addParking(commandString);
        }

        if("status".equals(command)){
            parkingReturn = getAllParking();
        }

        if(commandString.length == 2){
            String commandValue = commandString[1];
            
            if("parking_lot".equals(command)){
                parkingReturn = importData(commandValue);
            }

            if("create_parking_lot".equals(command)){
                parkingReturn = addParkingSlot(Integer.parseInt(commandValue));
            }

            if("registration_numbers_for_cars_with_colour".equals(command)){
                parkingReturn = getRegistrationNoByColour(commandValue);
            }

            if("slot_numbers_for_cars_with_colour".equals(command)){
                parkingReturn = getSlotNoByColour(commandValue);
            }

            if("slot_number_for_registration_number".equals(command)){
                parkingReturn = getSlotNoByRegistrationNo(commandValue);
            }

            if("leave".equals(command)){
                parkingReturn = leaveParking(Integer.parseInt(commandValue));
            }
        }
        return parkingReturn;
    }
    
    private static String[] getCommandLine(String text){
        return split(text ," ");
    }
    
    private static String[] split(String text,String separator){
        return text.split(separator);
    }
    
    public StringBuilder addParkingSlot(int parkingSlot){
        StringBuilder slotParking = new StringBuilder();
        if(this.slotNumber == 0){
            this.slotNumber = parkingSlot;
            slotParking.append("Created a parking lot with ");
            slotParking.append(parkingSlot);
            slotParking.append(" slots");
        }
        else{
            slotParking.append("Slot Parking already setup");
        }
        return slotParking;
    }
    
    public int getSlotNumber(){
        return slotNumber;
    } 
    
    public void setSlotNumber(int slotNumber){
        this.slotNumber = slotNumber;
    }
    
    public StringBuilder getAllParking(){
        StringBuilder dataParking = new StringBuilder();
        Parkings parkings = parkingDao.getAllParking();
        List<Parking> parkingList = parkings.getParkingList();
        Collections.sort(parkingList, new SortbyNo()); 
        dataParking.append("Slot No.\t");
        dataParking.append("Registration No\t");
        dataParking.append("Colour\n");
        for(int i = 0; i < parkingList.size(); i++){
            Parking parking = parkingList.get(i);
            dataParking.append(parking.getSlotNo());
            dataParking.append("\t");
            dataParking.append(parking.getRegistrationNo());
            dataParking.append("\t");
            dataParking.append(parking.getColor());
            dataParking.append("\n");
        }
        return dataParking;
    }
    
    public StringBuilder addParking(String[] command){
        StringBuilder allocate = new StringBuilder();
        Parkings parkings = parkingDao.getAllParking();
        List<Parking> parkingList = parkings.getParkingList();
        if(parkingList.size() >= this.slotNumber){
            allocate.append("Sorry, parking lot is full");
            return allocate;
        }
        Parking parking = new Parking();
        int slotNo = getAvaliableSlot();
        parking.setSlotNo(slotNo);
        parking.setRegistrationNo(command[1]);
        parking.setColor(command[2]);
        parkingDao.addParking(parking);
        allocate.append("Allocated slot number: ");
        allocate.append(slotNo);
        return allocate;
    }
    
    public StringBuilder leaveParking(int slotNo){
        StringBuilder leave = new StringBuilder();
        int parkingNo = 0;
        Parkings parkings = parkingDao.getAllParking();
        List<Parking> parkingList = parkings.getParkingList();
        Collections.sort(parkingList, new SortbyNo()); 
            for(int i = 0; i < parkingList.size(); i++){
                Parking parking = parkingList.get(i);
                parkingNo = parking.getSlotNo();
                if(parkingNo == slotNo)
                    parkingList.remove(i);
            }   
        leave.append("Slot number ");
        leave.append(slotNo);
        leave.append(" is free");
        return leave;
    }
    
    public StringBuilder getSlotNoByRegistrationNo(String registrationNo){
        String regisNo;
        Parkings parkings = parkingDao.getAllParking();
        List<Parking> parkingList = parkings.getParkingList();
        Collections.sort(parkingList, new SortbyNo()); 
            for(int i = 0; i < parkingList.size(); i++){
                Parking parking = parkingList.get(i);
                regisNo = parking.getRegistrationNo();
                if(regisNo.equals(registrationNo)) {
                    int slotNo = parking.getSlotNo();
                    return new StringBuilder().append(slotNo);
                }
            }   
            return new StringBuilder().append("Not found");
    }
    
    public StringBuilder getRegistrationNoByColour(String colour){
        String carColour;
        StringBuilder RegistrationNumber = new StringBuilder();
        Parkings parkings = parkingDao.getAllParking();
        List<Parking> parkingList = parkings.getParkingList();
        Collections.sort(parkingList, new SortbyNo()); 
            for(int i = 0; i < parkingList.size(); i++){
                Parking parking = parkingList.get(i);
                carColour = parking.getColor();
                if(carColour.equals(colour)) {
                    if(RegistrationNumber.length()>0)
                        RegistrationNumber.append(", ");
                    String regisNo = parking.getRegistrationNo();
                    RegistrationNumber.append(regisNo);
                }
            }
            return RegistrationNumber.length()== 0 ?new StringBuilder().append("Not found"): RegistrationNumber;
    }
    
    public StringBuilder getSlotNoByColour(String colour){
        String carColour;
        StringBuilder slotNumber = new StringBuilder();
        Parkings parkings = parkingDao.getAllParking();
        List<Parking> parkingList = parkings.getParkingList();
        Collections.sort(parkingList, new SortbyNo()); 
        for(int i = 0; i < parkingList.size(); i++){
            Parking parking = parkingList.get(i);
            carColour = parking.getColor();
            if(carColour.equals(colour)) {
                if(slotNumber.length()>0)
                    slotNumber.append(", ");
                int slotNo = parking.getSlotNo();
                slotNumber.append(slotNo);
            }
        }
        return slotNumber.length()== 0 ?new StringBuilder().append("Not found"): slotNumber;
}
    
    public int getAvaliableSlot(){
        int slotNo = 1;
        int useNo = 0;
        Parkings parkings = parkingDao.getAllParking();
        List<Parking> parkingList = parkings.getParkingList();
        for(int i = 0; i < parkingList.size(); i++){
            Parking parking = parkingList.get(i);
            useNo = parking.getSlotNo();
            for(int x = i+1; x < this.slotNumber; x++){
                if(useNo > x )
                    return x;
            }
            slotNo++;
        }
        return slotNo;
    }
    
    public StringBuilder importData(String filename) throws FileNotFoundException, IOException {
        StringBuilder dataImport = new StringBuilder();
        StringBuilder dataOutput = new StringBuilder();
        StringBuilder dataCommand = new StringBuilder(); 
        String systemPath = System.getProperty("user.dir");
        String filePath =  systemPath + "/functional_spec/fixtures/"+filename;
        
        FileReader fr = 
          new FileReader(filePath); 

        int i; 
        while ((i=fr.read()) != -1){ 
          if(i == 10){
              String output = parkingCommand(dataCommand.toString()).toString();
              dataOutput.append(output);
              dataOutput.append("\n");
              dataCommand = new StringBuilder();
          }
          else
            dataCommand.append((char) i);
          
          dataImport.append((char) i);
        }
        dataImport.append("\n");
        dataImport.append(dataOutput.toString());
        return dataImport;
    }
}

class SortbyNo implements Comparator<Parking> 
{ 
    public int compare(Parking a, Parking b) 
    { 
        return a.getSlotNo() - b.getSlotNo(); 
    } 
} 
