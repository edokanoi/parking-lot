/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mapan.parking_lot;

import com.mapan.parking_lot.controller.ParkingController;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author edyprayitno
 */
public class ParkingLotApplication {
    public static void main(String[] arrgs){
        
        BufferedReader br = null;
        ParkingController parkingController = new ParkingController();
        StringBuilder parkingReturn = new StringBuilder();
        try {

            br = new BufferedReader(new InputStreamReader(System.in));
            
            while (true) {
               
                System.out.print("Enter something : ");
                String input = br.readLine();
                parkingReturn = parkingController.parkingCommand(input);
                System.out.println(parkingReturn);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
        
}