/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mapan.parking_lot.dao;

import com.mapan.parking_lot.model.Parking;
import com.mapan.parking_lot.model.Parkings;

/**
 *
 * @author edyprayitno
 */
public class ParkingDAO {
    
    private static Parkings list = new Parkings();
    
    public Parkings getAllParking() 
    {
        return list;
    }
    
    public void addParking(Parking parking) {
        list.getParkingList().add(parking);
    }
    
}
