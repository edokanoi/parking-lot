/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mapan.parking_lot.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author edyprayitno
 */
public class Parkings {
    private ArrayList<Parking> parkingList;
    
    public List<Parking> getParkingList() {
        if(parkingList == null) {
            parkingList = new ArrayList<>();
        }
        return parkingList;
    }
 
    public void setProductList(ArrayList<Parking> parkingList) {
        this.parkingList = parkingList;
    }
    
    public List<Parking> getByRegistrationNo(String registrationNo){
        if(parkingList == null)
            System.out.println("Not Found");
        return parkingList;
    }
    
    public List<Parking> getByColor(String color){
        if(parkingList == null)
            System.out.println("Not Found");
        return (ArrayList)parkingList;
    }
}
