/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mapan.parking_lot.model;

/**
 *
 * @author edyprayitno
 */
public class Parking {
    
    public Parking() {

    }

    public Parking(int slotNo, String registrationNo, String color) {
        super();
        this.slotNo = slotNo;
        this.registrationNo = registrationNo;
        this.color = color;
    }
    
    private int slotNo;
    private String registrationNo;
    private String color;
    
    public int getSlotNo(){
        return slotNo;
    }
    
    public void setSlotNo(int slotNo){
        this.slotNo = slotNo;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    @Override
    public String toString() {
        return "Parking [registrationNo=" + registrationNo + ", color=" + color + "]";
    }
    
}
