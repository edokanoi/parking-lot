/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mapan.test;

/**
 *
 * @author edyprayitno
 */
import com.mapan.parking_lot.controller.ParkingController;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import static org.junit.Assert.assertThat;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class ParkingLotApplicationTest {

    ParkingController parkingController = new ParkingController();
    private StringBuilder parkingReturn = new StringBuilder();
    
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(ParkingLotApplicationTest.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
    }

    @Test
    public void createParkingLot() throws IOException {
        commandTest("create_parking_lot 6","Created a parking lot with 6 slots");        
        commandTest("park KA-01-HH-1234 White","Allocated slot number: 1");
        commandTest("park KA-01-HH-9999 White","Allocated slot number: 2");
        commandTest("park KA-01-BB-0001 Black","Allocated slot number: 3");
        commandTest("park KA-01-HH-7777 Red","Allocated slot number: 4");
        commandTest("park KA-01-HH-2701 Blue","Allocated slot number: 5");
        commandTest("park KA-01-HH-3141 Black","Allocated slot number: 6");
        commandTest("leave 4","Slot number 4 is free");        
        commandTest("park KA-01-P-333 White","Allocated slot number: 4");
        commandTest("park DL-12-AA-9999 White","Sorry, parking lot is full");
        commandTest("registration_numbers_for_cars_with_colour White","KA-01-HH-1234, KA-01-HH-9999, KA-01-P-333");
        commandTest("slot_numbers_for_cars_with_colour White","1, 2, 4");
        commandTest("slot_number_for_registration_number KA-01-HH-3141","6");
        commandTest("slot_number_for_registration_number MH-04-AY-1111","Not found");
        }
    
    @Test
    public void testImport() throws IOException{
        commandTest("leave 1","Slot number 1 is free");
        commandTest("leave 2","Slot number 2 is free");  
        commandTest("leave 3","Slot number 3 is free");  
        commandTest("leave 4","Slot number 4 is free");  
        commandTest("leave 5","Slot number 5 is free");  
        commandTest("leave 6","Slot number 6 is free");  
        try {
            parkingReturn = parkingController.importData("file_input.txt");
            System.out.print(this.parkingReturn);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ParkingLotApplicationTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public void commandTest(String parkingCommand, String expectation) throws IOException{
        this.parkingReturn = parkingController.parkingCommand(parkingCommand);
        assertThat(this.parkingReturn.toString(), is(expectation));
    }
    
}
